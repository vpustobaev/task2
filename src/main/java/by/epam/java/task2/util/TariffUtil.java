package by.epam.java.task2.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import by.epam.java.task2.entity.Tariff;
import by.epam.java.task2.entity.TariffWithMaintenanceFee;

public class TariffUtil {

    public void sortTariffs(ArrayList<Tariff> tariffList) {

	Collections.sort(tariffList, new Comparator<Tariff>() {

	    @Override
	    public int compare(Tariff t1, Tariff t2) {

		return ((TariffWithMaintenanceFee) t1).getMaintenanceFee()
			.compareTo(((TariffWithMaintenanceFee) t2).getMaintenanceFee());
	    }

	});

    }

    public TariffWithMaintenanceFee findMostSuitableTariffByMaintenanceFee(BigDecimal moneyReadyToPay,
	    ArrayList<Tariff> tariffList) {

	for (Tariff tariff : tariffList) {

	    if (tariff instanceof TariffWithMaintenanceFee) {

		if (((TariffWithMaintenanceFee) tariff).getMaintenanceFee().compareTo(moneyReadyToPay) <= 0) {

		    return (TariffWithMaintenanceFee) tariff;
		}

	    }
	}

	return null;

    }

}
