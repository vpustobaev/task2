package by.epam.java.task2.entity;

import java.util.ArrayList;
import java.math.BigDecimal;
import by.epam.java.task2.exception.ArgumentNullException;
import by.epam.java.task2.util.TariffUtil;

public class Main {

    public static void main(String[] args) {

	TariffFactory factory = new TariffFactory();

	Tariff tariff1 = null;
	Tariff tariff2 = null;
	Tariff tariff3 = null;
	Tariff tariff4 = null;
	Tariff tariff5 = null;
	Tariff tariff6 = null;

	try {
	    tariff1 = factory.createYo1200();
	    tariff2 = factory.createStartYourLife();
	    tariff3 = factory.createM();
	    tariff4 = factory.createL();
	    tariff5 = factory.createXs();
	    tariff6 = factory.createS();
	} catch (ArgumentNullException e) {
	    e.printStackTrace();
	}

	ArrayList<Tariff> tariffs = new ArrayList<>();
	tariffs.add(tariff3);
	tariffs.add(tariff4);
	tariffs.add(tariff2);
	tariffs.add(tariff1);
	tariffs.add(tariff5);
	tariffs.add(tariff6);
	tariffs.trimToSize();

	TariffUtil util = new TariffUtil();

	TariffWithMaintenanceFee suitableTariff = util.findMostSuitableTariffByMaintenanceFee(new BigDecimal(5),
		tariffs);

	System.out.println("The most suitable Tariff for the client is " + suitableTariff.name);
	System.out.println();

	ArrayList<Tariff> tariffsWithMaintance = new ArrayList<>();
	tariffsWithMaintance.add(tariff6);
	tariffsWithMaintance.add(tariff4);
	tariffsWithMaintance.add(tariff3);
	tariffsWithMaintance.add(tariff5);
	tariffsWithMaintance.trimToSize();
	util.sortTariffs(tariffsWithMaintance);

	for (int i = 0; i < tariffsWithMaintance.size(); i++) {

	    System.out.println(tariffsWithMaintance.get(i));

	}

	ArrayList<Client> clients = new ArrayList<>();

	Client client1 = new Client("Vasia");
	Client client2 = new Client("Anna");
	Client client3 = new Client("Petia");
	Client client4 = new Client("Katia");

	clients.add(client1);
	clients.add(client2);
	clients.add(client3);
	clients.add(client4);

	System.out.println();
	System.out.println("The mobile company has " + clients.size() + " clients");

    }

}
