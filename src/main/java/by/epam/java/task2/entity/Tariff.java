package by.epam.java.task2.entity;

import java.math.BigDecimal;

public class Tariff {

    protected String name;
    protected BigDecimal callPrice;
    protected BigDecimal trafficPrice;

    public Tariff(String name, BigDecimal callPrice, BigDecimal trafficPrice) {
	this.name = name;
	this.callPrice = callPrice;
	this.trafficPrice = trafficPrice;
    }

    public BigDecimal getCallPrice() {
	return callPrice;
    }

    public BigDecimal getTrafficPrice() {
	return trafficPrice;
    }

    @Override
    public String toString() {
	return "Tariff name=" + name + ", call price=" + callPrice.doubleValue() + ", traffic price="
		+ trafficPrice.doubleValue();
    }

}
