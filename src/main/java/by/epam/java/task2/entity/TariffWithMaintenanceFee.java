package by.epam.java.task2.entity;

import java.math.BigDecimal;
import java.util.Arrays;

public class TariffWithMaintenanceFee extends Tariff {

    private BigDecimal maintenanceFee;
    private IPackage[] packages;

    public TariffWithMaintenanceFee(String name, BigDecimal callPrice, BigDecimal trafficPrice,
	    BigDecimal maintenanceFee, IPackage[] packages) {
	super(name, callPrice, trafficPrice);
	this.maintenanceFee = maintenanceFee;
	this.packages = packages;

    }

    public BigDecimal getMaintenanceFee() {
	return maintenanceFee;
    }

    public void setMaintenanceFee(BigDecimal maintenanceFee) {
	this.maintenanceFee = maintenanceFee;
    }

    public IPackage[] getPackages() {
	return packages;
    }

    @Override
    public String toString() {
	return super.toString() + ", maintenance fee=" + maintenanceFee.doubleValue() + ", packages="
		+ Arrays.toString(packages);
    }

}
