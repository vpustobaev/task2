package by.epam.java.task2.entity.internetpackage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.epam.java.task2.entity.IPackage;
import by.epam.java.task2.exception.ArgumentNullException;

public class IncludedDailyTrafficInternetPackage implements IPackage {

    private static final Logger logger = LogManager.getLogger(IncludedDailyTrafficInternetPackage.class);
    private static final String NO_TRAFFIC_SPECIFIED = "Please specify traffic amount";

    private int traffic;

    public IncludedDailyTrafficInternetPackage(int traffic) throws ArgumentNullException {
	if (traffic < 1) {
	    logger.error(NO_TRAFFIC_SPECIFIED);
	    throw new ArgumentNullException(NO_TRAFFIC_SPECIFIED);
	}

	this.traffic = traffic;
    }

    public String toString() {
	return "Includes " + this.traffic + " Mb per day";
    }

}
