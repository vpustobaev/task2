package by.epam.java.task2.entity;

public class Client {

    private String name;

    public Client(String name) {

	this.setName(name);

    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

}
