package by.epam.java.task2.entity.callpackage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import by.epam.java.task2.entity.IPackage;
import by.epam.java.task2.exception.ArgumentNullException;

public class IncludedDailyMinutesCallPackage implements IPackage {

    private static final Logger logger = LogManager.getLogger(IncludedDailyMinutesCallPackage.class);
    private static final String NO_MINUTES_SPECIFIED = "Please specify minutes amount";

    private int minutes;

    public IncludedDailyMinutesCallPackage(int minutes) throws ArgumentNullException {
	if (minutes < 0) {
	    logger.error(NO_MINUTES_SPECIFIED);
	    throw new ArgumentNullException(NO_MINUTES_SPECIFIED);
	}

	this.minutes = minutes;
    }

    public int getMinutes() {
	return minutes;
    }

    public void setMinutes(int minutes) {
	this.minutes = minutes;
    }

    public String toString() {
	return "Includes " + this.minutes + " minutes per day";
    }

}
