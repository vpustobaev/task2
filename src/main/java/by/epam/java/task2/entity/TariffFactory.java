package by.epam.java.task2.entity;

import java.math.BigDecimal;
import by.epam.java.task2.entity.callpackage.IncludedMonthlyMinutesCallPackage;
import by.epam.java.task2.entity.internetpackage.IncludedMonthlyTrafficInternetPackage;
import by.epam.java.task2.exception.ArgumentNullException;

public class TariffFactory {

    public Tariff createYo1200() throws ArgumentNullException {

	Tariff result = new Tariff("Yo 1200", new BigDecimal(0.075), new BigDecimal(0.24));

	return result;

    }

    public Tariff createStartYourLife() throws ArgumentNullException {

	Tariff result = new Tariff("Start Your Life", new BigDecimal(0.06), new BigDecimal(0.24));

	return result;

    }

    public TariffWithMaintenanceFee createXs() throws ArgumentNullException {

	IPackage[] packages = { new IncludedMonthlyTrafficInternetPackage(600),
		new IncludedMonthlyMinutesCallPackage(60) };

	TariffWithMaintenanceFee result = new TariffWithMaintenanceFee("XS", new BigDecimal(0), new BigDecimal(0.24),
		new BigDecimal(3.5), packages);

	return result;

    }

    public TariffWithMaintenanceFee createS() throws ArgumentNullException {

	IPackage[] packages = { new IncludedMonthlyTrafficInternetPackage(1200),
		new IncludedMonthlyMinutesCallPackage(120) };

	TariffWithMaintenanceFee result = new TariffWithMaintenanceFee("S", new BigDecimal(0), new BigDecimal(0),
		new BigDecimal(7), packages);

	return result;

    }

    public TariffWithMaintenanceFee createM() throws ArgumentNullException {

	IPackage[] packages = { new IncludedMonthlyTrafficInternetPackage(1000),
		new IncludedMonthlyMinutesCallPackage(150) };

	TariffWithMaintenanceFee result = new TariffWithMaintenanceFee("M", new BigDecimal(0), new BigDecimal(0),
		new BigDecimal(8.4), packages);

	return result;

    }

    public TariffWithMaintenanceFee createL() throws ArgumentNullException {

	IPackage[] packages = { new IncludedMonthlyTrafficInternetPackage(2000),
		new IncludedMonthlyMinutesCallPackage(300) };

	TariffWithMaintenanceFee result = new TariffWithMaintenanceFee("L", new BigDecimal(0), new BigDecimal(0),
		new BigDecimal(12.5), packages);

	return result;

    }

}
