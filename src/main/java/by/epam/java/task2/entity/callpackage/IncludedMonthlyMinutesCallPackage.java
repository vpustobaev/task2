package by.epam.java.task2.entity.callpackage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.epam.java.task2.entity.IPackage;
import by.epam.java.task2.exception.ArgumentNullException;

public class IncludedMonthlyMinutesCallPackage implements IPackage {

    private static final Logger logger = LogManager.getLogger(IncludedMonthlyMinutesCallPackage.class);
    private static final String NO_MINUTES_SPECIFIED = "Please specify minutes amount";

    private int minutes;

    public IncludedMonthlyMinutesCallPackage(int minutes) throws ArgumentNullException {
	if (minutes < 1) {

	    logger.error(NO_MINUTES_SPECIFIED);
	    throw new ArgumentNullException(NO_MINUTES_SPECIFIED);
	}

	this.minutes = minutes;
    }

    public String toString() {
	return "Includes " + this.minutes + " minutes per month";
    }

}
