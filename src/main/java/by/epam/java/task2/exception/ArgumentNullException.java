package by.epam.java.task2.exception;

public class ArgumentNullException extends Exception {

    public ArgumentNullException(String message) {
	super(message);

    }

}
